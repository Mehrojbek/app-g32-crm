package uz.pdp.appg32crm.exception;

public class RestException extends RuntimeException{

    public static RestException restThrow(String msg){
        return new RestException(msg);
    }

    public static RestException notFound(String msg){
        return new RestException(msg+" not found");
    }

    public RestException(String message) {
        super(message);
    }

    public static RestException alreadyExist(String key) {
        return new RestException(key + " already exists");
    }
}
