package uz.pdp.appg32crm.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LeadCreateUpdateDTO {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Long statusId;

    private double budget;

}
