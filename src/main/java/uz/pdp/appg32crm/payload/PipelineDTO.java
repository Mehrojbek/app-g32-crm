package uz.pdp.appg32crm.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PipelineDTO {

    private Long id;

    private String name;

}
