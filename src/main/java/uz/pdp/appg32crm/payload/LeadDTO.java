package uz.pdp.appg32crm.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appg32crm.entity.Status;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LeadDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private StatusDTO status;

    private double budget;
}
