package uz.pdp.appg32crm.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appg32crm.enums.StatusTypeEnum;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatusDTO {

    private Long id;

    private String name;

    private StatusTypeEnum type;

    private Long pipelineId;

}
