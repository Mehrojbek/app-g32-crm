package uz.pdp.appg32crm.entity;

import lombok.*;

import java.io.Serializable;

/**
 * Voronka misol uchun sales, call center vahokazo lar uchun
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Pipeline extends AbsEntity implements Serializable {

    private String name;

}
