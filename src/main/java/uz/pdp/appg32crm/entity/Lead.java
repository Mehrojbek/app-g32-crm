package uz.pdp.appg32crm.entity;

import lombok.*;

import java.io.Serializable;

/**
 * Potensial mijoz.
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Lead extends AbsEntity implements Serializable {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Status status;

    private double budget;
}
