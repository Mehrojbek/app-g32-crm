package uz.pdp.appg32crm.entity;

import lombok.*;
import uz.pdp.appg32crm.enums.StatusTypeEnum;

/**
 * Shu lead qaysi statusda turganini bildirish uchun.
 * Misol uchun qoralama, o'ylab ko'radi, kurs sotib olmoqchi vahokazo
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Status extends AbsEntity {

    private String name;

    private Pipeline pipeline;

    private StatusTypeEnum type;
}
