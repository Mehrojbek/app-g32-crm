package uz.pdp.appg32crm.test;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.github.javafaker.PhoneNumber;
import com.github.javafaker.service.RandomService;
import uz.pdp.appg32crm.enums.StatusTypeEnum;
import uz.pdp.appg32crm.payload.LeadCreateUpdateDTO;
import uz.pdp.appg32crm.payload.LeadDTO;
import uz.pdp.appg32crm.payload.PipelineDTO;
import uz.pdp.appg32crm.payload.StatusDTO;
import uz.pdp.appg32crm.service.LeadServiceImpl;
import uz.pdp.appg32crm.service.PipelineServiceImpl;
import uz.pdp.appg32crm.service.StatusServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectTest {

    public static void main(String[] args) {

        List<PipelineDTO> pipelineDTOList = createPipelines();

//        pipelineDTOList.forEach(System.out::println);

        Map<PipelineDTO, List<StatusDTO>> statusMap = createStatuses(pipelineDTOList);

        List<StatusDTO> statusDTOList = statusMap.values()
                .stream()
                .flatMap(statusDTOS -> statusDTOS.stream())
                .collect(Collectors.toList());

        Map<PipelineDTO, Map<StatusDTO, List<LeadDTO>>> leadMap = createLeads(statusDTOList);

//        statusMap.forEach((pipelineDTO, statusDTOS) -> {
//            System.out.println();
//            System.out.println(pipelineDTO + " ========>>>>>>>\n");
//            statusDTOS.forEach(System.out::println);
//        });

    }

    private static Map<PipelineDTO, Map<StatusDTO, List<LeadDTO>>> createLeads(List<StatusDTO> statusDTOList) {

        Faker faker = Faker.instance();

        Name name = faker.name();

        PhoneNumber phoneNumber = faker.phoneNumber();

        RandomService random = faker.random();

        LeadServiceImpl leadService = LeadServiceImpl.getInstance();

        System.out.println("dshgvsdghvcghsdvchg");

        List<LeadDTO> leadDTOList1 = statusDTOList
                .stream()
                .map(statusDTO -> {
                    int countOfLeads = random.nextInt(10, 40);

                    List<LeadCreateUpdateDTO> leadDTOList = new ArrayList<>();
                    for (int i = 0; i < countOfLeads; i++) {

                        leadDTOList.add(new LeadCreateUpdateDTO(
                                name.firstName(),
                                name.lastName(),
                                phoneNumber.phoneNumber(),
                                statusDTO.getId(),
                                random.nextInt(60, 800)
                        ));
                    }

                    return leadDTOList;
                })
                .flatMap(leadCreateUpdateDTOS -> leadCreateUpdateDTOS.stream())
                .map(leadCreateUpdateDTO -> leadService.add(leadCreateUpdateDTO))
                .collect(Collectors.toList());


        return null;
    }

    private static Map<PipelineDTO, List<StatusDTO>> createStatuses(List<PipelineDTO> pipelineDTOList) {

        Map<PipelineDTO, List<StatusDTO>> statusMap = new HashMap<>();

        for (PipelineDTO pipelineDTO : pipelineDTOList) {

            List<StatusDTO> statusDTOList = List.of(
                    new StatusDTO(
                            null,
                            "draft",
                            StatusTypeEnum.DRAFT,
                            pipelineDTO.getId()
                    ),
                    new StatusDTO(
                            null,
                            "lost",
                            StatusTypeEnum.LOST,
                            pipelineDTO.getId()
                    ),
                    new StatusDTO(
                            null,
                            "win",
                            StatusTypeEnum.WIN,
                            pipelineDTO.getId()
                    ),
                    new StatusDTO(
                            null,
                            "pul to'lamoqchi",
                            StatusTypeEnum.CUSTOM,
                            pipelineDTO.getId()
                    )
            );

            StatusServiceImpl statusService = StatusServiceImpl.getInstance();

            List<StatusDTO> statusDTOListOfPipeline = statusDTOList
                    .stream()
                    .map(statusDTO -> statusService.add(statusDTO))
                    .collect(Collectors.toList());

            statusMap.put(pipelineDTO, statusDTOListOfPipeline);
        }

        return statusMap;
    }

    private static List<PipelineDTO> createPipelines() {

        List<PipelineDTO> pipelineDTOList = List.of(
                new PipelineDTO(
                        null,
                        "Sales"
                ),
                new PipelineDTO(
                        null,
                        "Call center"
                ),
                new PipelineDTO(
                        null,
                        "Admin"
                )
        );

        PipelineServiceImpl pipelineService = PipelineServiceImpl.getInstance();

        List<PipelineDTO> result = pipelineDTOList
                .stream()
                .map(pipelineDTO -> pipelineService.add(pipelineDTO))
                .collect(Collectors.toList());

        return result;
    }

}
