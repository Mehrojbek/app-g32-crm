package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.entity.Pipeline;
import uz.pdp.appg32crm.payload.PipelineDTO;
import uz.pdp.appg32crm.payload.PipelineEditDTO;

import java.util.List;

public interface PipelineService {

    PipelineDTO add(PipelineDTO pipelineDTO);

    List<PipelineDTO> read();

    PipelineDTO edit(Long id, PipelineEditDTO pipelineEditDTO);

    boolean delete(Long id);

    Pipeline getByIdOrElseThrow(Long id);
}
