package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.payload.LeadCreateUpdateDTO;
import uz.pdp.appg32crm.payload.LeadDTO;

import java.util.List;

public interface LeadService {

    List<LeadDTO> readByPipelineId(Long pipelineId);

    LeadDTO add(LeadCreateUpdateDTO createDTO);

    LeadDTO edit(Long id, LeadCreateUpdateDTO updateDTO);

    boolean delete(Long id);
}
