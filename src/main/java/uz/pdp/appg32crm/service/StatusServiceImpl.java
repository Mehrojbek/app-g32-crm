package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.entity.Pipeline;
import uz.pdp.appg32crm.entity.Status;
import uz.pdp.appg32crm.enums.StatusTypeEnum;
import uz.pdp.appg32crm.exception.RestException;
import uz.pdp.appg32crm.payload.StatusDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StatusServiceImpl implements StatusService {

    private static StatusServiceImpl instance;
    public List<Status> statuses = new ArrayList<>();

    private StatusServiceImpl() {

    }

    public static StatusServiceImpl getInstance() {
        if (Objects.isNull(instance))
            instance = new StatusServiceImpl();
        return instance;
    }

    @Override
    public StatusDTO add(StatusDTO statusDTO) {

        PipelineServiceImpl pipelineService = PipelineServiceImpl.getInstance();

        Pipeline pipeline = pipelineService.getByIdOrElseThrow(statusDTO.getPipelineId());

        boolean anyMatch = statuses
                .stream()
                .anyMatch(status -> Objects.equals(status.getName(), statusDTO.getName())
                        && Objects.equals(status.getPipeline().getId(), statusDTO.getPipelineId()));

        if (anyMatch)
            throw RestException.alreadyExist("Status");

        Status status = new Status(
                statusDTO.getName(),
                pipeline,
                StatusTypeEnum.CUSTOM
        );

        status.setId((long) (statuses.size()+1));
        statuses.add(status);

        return toDTO(status);
    }

    @Override
    public List<StatusDTO> read(Long pipelineId) {
        return statuses
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public StatusDTO edit(Long id, StatusDTO statusDTO) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public StatusDTO toDTO(Status status) {
        return new StatusDTO(
                status.getId(),
                status.getName(),
                status.getType(),
                status.getPipeline().getId()
        );
    }

    @Override
    public Status getByIdOrElseThrow(Long id) {
        return statuses
                .stream()
                .filter(status -> Objects.equals(status.getId(), id))
                .findFirst()
                .orElseThrow(() -> RestException.notFound("status"));
    }
}
