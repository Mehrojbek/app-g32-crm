package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.entity.Status;
import uz.pdp.appg32crm.payload.StatusDTO;

import java.util.List;

public interface StatusService {

    StatusDTO add(StatusDTO statusDTO);

    List<StatusDTO> read(Long pipelineId);

    StatusDTO edit(Long id, StatusDTO statusDTO);

    boolean delete(Long id);

    StatusDTO toDTO(Status status);

    Status getByIdOrElseThrow(Long id);
}
