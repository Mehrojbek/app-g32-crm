package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.entity.Lead;
import uz.pdp.appg32crm.entity.Status;
import uz.pdp.appg32crm.payload.LeadCreateUpdateDTO;
import uz.pdp.appg32crm.payload.LeadDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LeadServiceImpl implements LeadService {
    private static LeadServiceImpl instance;

    public List<Lead> leads = new ArrayList<>();

    private LeadServiceImpl() {

    }

    public static LeadServiceImpl getInstance() {
        if (Objects.isNull(instance))
            instance = new LeadServiceImpl();
        return instance;
    }

    @Override
    public List<LeadDTO> readByPipelineId(Long pipelineId) {
        return leads
                .stream()
                .filter(lead -> Objects.equals(lead.getStatus().getPipeline().getId(),pipelineId))
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public LeadDTO add(LeadCreateUpdateDTO createDTO) {

        StatusServiceImpl statusService = StatusServiceImpl.getInstance();

        Status status = statusService.getByIdOrElseThrow(createDTO.getStatusId());

        Lead lead = new Lead(
                createDTO.getFirstName(),
                createDTO.getLastName(),
                createDTO.getPhoneNumber(),
                status,
                createDTO.getBudget()
        );
        lead.setId((long) (leads.size()+1));

        leads.add(lead);

        return toDTO(lead);
    }

    @Override
    public LeadDTO edit(Long id, LeadCreateUpdateDTO updateDTO) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    private LeadDTO toDTO(Lead lead) {
        StatusServiceImpl statusService = StatusServiceImpl.getInstance();
        return new LeadDTO(
                lead.getId(),
                lead.getFirstName(),
                lead.getLastName(),
                lead.getPhoneNumber(),
                statusService.toDTO(lead.getStatus()),
                lead.getBudget()
        );
    }
}
