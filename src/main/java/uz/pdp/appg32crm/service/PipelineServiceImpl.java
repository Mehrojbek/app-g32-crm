package uz.pdp.appg32crm.service;

import uz.pdp.appg32crm.entity.Pipeline;
import uz.pdp.appg32crm.exception.RestException;
import uz.pdp.appg32crm.payload.PipelineDTO;
import uz.pdp.appg32crm.payload.PipelineEditDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PipelineServiceImpl implements PipelineService {
    private static PipelineServiceImpl instance;

    public List<Pipeline> pipelines = new ArrayList<>();

    private PipelineServiceImpl() {

    }

    public static PipelineServiceImpl getInstance() {
        if (Objects.isNull(instance))
            instance = new PipelineServiceImpl();
        return instance;
    }


    @Override
    public PipelineDTO add(PipelineDTO pipelineDTO) {

        boolean anyMatch = pipelines
                .stream()
                .anyMatch(pipeline -> Objects.equals(pipeline.getName(), pipelineDTO.getName()));

        if (anyMatch)
            throw RestException.restThrow("Pipeline already exist");

        Pipeline pipeline = new Pipeline(pipelineDTO.getName());
        pipeline.setId((long) (pipelines.size() + 1));
        pipelines.add(pipeline);

        return toDTO(pipeline);
    }

    @Override
    public List<PipelineDTO> read() {
        return pipelines
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public PipelineDTO edit(Long id, PipelineEditDTO pipelineEditDTO) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public Pipeline getByIdOrElseThrow(Long id) {
        return pipelines
                .stream()
                .filter(pipeline -> Objects.equals(pipeline.getId(),id))
                .findFirst().orElseThrow(() -> RestException.notFound("Pipeline"));
    }

    private PipelineDTO toDTO(Pipeline pipeline) {
        return new PipelineDTO(
                pipeline.getId(),
                pipeline.getName()
        );
    }
}
