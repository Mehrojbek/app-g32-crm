package uz.pdp.appg32crm.enums;

public enum StatusTypeEnum {

    //ISHLOV BERILGAN POTENSIAL MIJOZ
    DRAFT,

    //USERLAR TOMONIDAN YARATILGAN STATUSLAR
    CUSTOM,

    //YUTQAZILGAN MIJOZ
    LOST,

    //YUTILGAN MIJOZ
    WIN


}
